#ifndef _LIST_H_
#define _LIST_H_

int empty_list(list ptrList);//Function that checks if list is empty

int list_search_id(int id,list ptrList);//Function that searches all the books list to find if the id parameter matches with an id from list nodes

list list_init(void);//Function that creates a list and initializes it

void print_list(list ptrList);//Function that prints all the list of books


#endif