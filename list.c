#include <stdio.h>
#include <stdlib.h>
#include "books.h"

int empty_list(list ptrList)//Function that checks if list is empty
{
    return ptrList -> head == NULL;//Returns the result of that comparison 0:False !=0: True
}


int list_search_id(int id,list ptrList)//Function that searches all the books list to find if the id parameter matches with an id from list nodes
{
    node current = NULL;
    if(empty_list(ptrList))
    {
        return 0;
    } else {
        current = ptrList -> head;
        while( current != NULL )
        {
            if( current -> data.id == id ){
                printf("%s","\nThe id you inserted already exists!\n");
                return 1;//returns true value if id already exists
                break;
            }
            current = current -> next;
            }
    }
    return 0;
}




list list_init(void)//Function that creates a list and initializes it
{
    list l = (list)malloc(sizeof(struct _list));
    if(l == NULL)
    {
        puts("Memory allocation failed,list can't be created");
    } else {
        l -> head = NULL;
        l -> size = 0;
        return l;
    }
}

void print_list(list ptrList)//Function that prints all the list of books
{
    node current = NULL;
    book tmp = {"","",0,0,""};

    current = ptrList -> head;
    while( current != NULL )
    {
        tmp.id = current -> data.id;
        tmp = findBook(tmp,ptrList);
        print(tmp);
        current = current -> next;
    }
}