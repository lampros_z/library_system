#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "books.h"
#include "list.h"

int main(int argc,char **argv)//Code never lies,comments sometimes do
{
    if( argc != 2)  //Protection from wrong argument input
    {
        puts("Please run the program again and give me the name of the file you wish to save the data");
        exit(EXIT_FAILURE);
    }
    

    unsigned int choice;
    int id , exit_flag = 0 , func_flag_add = 0 , func_flag_del = 0 ,func_flag_up = 0 ;
    char choice_p,c;
   
   list l = load(argv[1]);//Here main function calls load function to recover books list from savefile.If program runs for first time or file delted message will print to user
    
    printMenu();//Here main function calls print function to print the basic menu for user

    do{
        printf("%s","Please select a number from the menu above");//Message for user
        printf("%s","? ");
    } while ( ( (scanf("%u%c", &choice, &c)!=2 || c!='\n') && clean_stdin() ) );//Protection from wrong input(code taken from stack overflow). 
//in this case scanf should read only 2 elements an integer number and \n char,scanf retruns the number of elements read successfully,we check that number,if first member of condition is false then then the second member is skipped,second member just cleans the standard input and retuns 1(true)


    while( choice != 7 )
    {
        book tmp = {"","",0,0,""};//varible type struct book,This variable is just for initialization
        switch(choice)
        {
            case 1:
            printf("You chosen to add a new book\n");
            puts("Do you wish to continue,answer with y (for yes) or n (for no) ");
            do{
                printf("%s","? ");
                scanf(" %c",&choice_p);
                choice_p = (char)tolower(choice_p);//convert uppercase character to lowercase to suffice programs concept 
                if( choice_p != 'y' && choice_p != 'n'){//Prtotection for wrong input
                    printf("%s","Please answer only with y or n\n");
                    clean_stdin();
                }
             }while( choice_p != 'y' && choice_p != 'n');
            if( choice_p == 'y')
            {
            do{
                printf("%s","Please give me the id of the book you want to regiter: ");
                do{
                    printf("%s"," ?");
                } while ( ( (scanf("%d%c", &id,&c)!=2 || c!='\n') && clean_stdin() ) );
            exit_flag = list_search_id(id,l);//Here main function calls list_search_id function,for explanation please see list.c file
            } while(exit_flag);
            tmp = createBook(tmp,id);//Here main function calls create book function,for explanation please see books.c file
            func_flag_add = addBook(tmp,l);
            if( func_flag_add )
            {
                puts("Book added with success");
            } else {
                puts("Book not registered.Memory allocation failed.");
            }
            }
            break;

            case 2:
            if( l -> size != 0)//check if list is empty
            {
                printf("You chosen to search if a specific book is registered\n");
                puts("Do you wish to continue,answer with y (for yes) or n (for no) ");
                do{
                printf("%s","? ");
                scanf(" %c",&choice_p);
                choice_p = (char)tolower(choice_p);
                if( choice_p != 'y' && choice_p != 'n'){
                    printf("%s","Please answer only with y or n\n");
                    clean_stdin();
                }
                }while( choice_p != 'y' && choice_p != 'n');
                if( choice_p == 'y')
                {
                    printf("%s","Please enter the id of the book you want to search.\n");
                    do{
                        printf("%s"," ?");
                    } while ( ( (scanf("%d%c", &id,&c)!=2 || c!='\n') && clean_stdin() ) );//Protection for wrong input
                    tmp.id = id;
                    findBook( tmp , l );
                }   
            } else {
                puts("List is empty!");
            }
            break;

            case 3:
            if( l -> size != 0)//check if list is empty
            {
                printf("You chosen to delete a book\n");
                puts("Do you wish to continue,answer with y (for yes) or n (for no) ");
                do{
                    printf("%s","? ");
                    scanf(" %c",&choice_p);
                    choice_p = (char)tolower(choice_p);
                    if( choice_p != 'y' && choice_p != 'n'){
                        printf("%s","Please answer only with y or n\n");
                        clean_stdin();
                    }
                }while( choice_p != 'y' && choice_p != 'n');
                if( choice_p == 'y')
                {
                    printf("%s","Please enter the id of the book you want to delete.\n");
                    do{
                        printf("%s"," ?");
                    } while ( ( (scanf("%d%c", &id,&c)!=2 || c!='\n') && clean_stdin() ) );
                    tmp.id = id;
                    tmp = findBook( tmp , l );
                    func_flag_del = deleteBook( tmp , l );
                    if( func_flag_del )
                    {
                        puts("Book deleted with success");
                    } else {
                        puts("Nothing deleted,probably no id matched,check for error message above.");
                    }
                }
            } else {
                puts("List is empty!!");
            }
            break;

            case 4:
            if( l -> size != 0)
            {
                printf("You chosen to update a book\n");
                puts("Do you wish to continue,answer with y (for yes) or n (for no) ");
                do{
                    printf("%s","? ");
                    scanf(" %c",&choice_p);
                    choice_p = (char)tolower(choice_p);
                    if( choice_p != 'y' && choice_p != 'n'){
                        printf("%s","Please answer only with y or n\n");
                        clean_stdin();
                    }
                }while( choice_p != 'y' && choice_p != 'n');
                if( choice_p == 'y')
                {
                    printf("%s","Please enter the id of the book you want to update.\n");
                    do{
                    printf("%s"," ?");
                    } while ( ( (scanf("%d%c", &id,&c)!=2 || c!='\n') && clean_stdin() ) );
                    tmp.id = id;
                    tmp = findBook( tmp , l );
                    func_flag_up = updateBook( tmp , l );
                    if( func_flag_up )
                    {
                        puts("Book updated with success");
                    } else {
                        puts("Nothing updated,probably no id matched,check for error message above.");
                    }
                }
            } else {
                puts("List is empty!!");
            }
            break;

            case 5:
            if( l -> size != 0)
            {
                printf("You chosen to print a book\n");
                puts("Do you wish to continue,answer with y (for yes) or n (for no) ");
                do{
                    printf("%s","? ");
                    scanf(" %c",&choice_p);
                    choice_p = (char)tolower(choice_p);
                    if( choice_p != 'y' && choice_p != 'n'){
                        printf("%s","Please answer only with y or n\n");
                        clean_stdin();
                    }
                }while( choice_p != 'y' && choice_p != 'n');
                if( choice_p == 'y')
                {
                    printf("%s","Please enter the id of the book you want to print.\n");
                    do{
                        printf("%s"," ?");
                    } while ( ( (scanf("%d%c", &id,&c)!=2 || c!='\n') && clean_stdin() ) );
                    tmp.id = id;
                    tmp = findBook( tmp , l );
                    print(tmp);
                }
            } else {
                puts("List is empty!");
            }
            break;

            case 6:
            if( l -> size != 0)
            {
                printf("You chosen to print a list that contains all the books\n");
                    puts("Do you wish to continue,answer with y (for yes) or n (for no) ");
                    do{
                    printf("%s","? ");
                    scanf(" %c",&choice_p);
                    choice_p = (char)tolower(choice_p);
                    if( choice_p != 'y' && choice_p != 'n'){
                        printf("%s","Please answer only with y or n\n");
                        clean_stdin();
                    }
                }while( choice_p != 'y' && choice_p != 'n');
                if( choice_p == 'y')
                print_list( l );
            } else {
                puts("List is empty!");
            }
            break;
            

            default :
            puts("Incorrect choice,try again");
            break;
        }
        
        printMenu();
        printf("%s","Please select a number from the menu above");
        do{
            printf("%s","? ");
        } while ( ( (scanf("%u%c", &choice, &c)!=2 || c!='\n') && clean_stdin() ) );
    }
    if( l -> size != 0)//Checks if list is empty
    {
        if( func_flag_add || func_flag_del || func_flag_up )//Here i check if func_flag is true,that value returned !!only from the functions that modify the list of books!!not all functions just addBook,updateBook,removeBook
        {//Please see books.c file,at that file i explain exactly how this value occurs
            puts("You are about to exit the program");//if that value is true means that something modified and program must save the changes
            puts("There are unsaved changes do you wish to save");
            puts("Answer with y (for yes) or n (for no) ");
            do{
                printf("%s","? ");
                scanf(" %c",&choice_p);
                if( choice_p != 'y' && choice_p != 'n'){
                    printf("%s","Please answer only with y or n\n");
                    clean_stdin();
                }
            }while( choice_p != 'y' && choice_p != 'n');
            if( choice_p == 'y')
            save( argv[1] , l );//Here main function calls save function to save unsaved changes
        }
    } else
    {
        puts("List is empty,there is nothing to save");
    }
}
