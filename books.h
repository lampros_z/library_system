#ifndef _BOOKS_H_
#define _BOOKS_H_

#define MAXSTRING 1024
#define MAXREVIEWS 10

enum genres
{
    FICTION = 1,
    SCIENTIFIC,
    POLITICS
};

typedef struct{
   char author[MAXSTRING];
   char title[MAXSTRING];
   enum genres genre;
   int id;
   char reviews[MAXREVIEWS][MAXSTRING];
}book;

typedef struct _node *node;

struct _node
{
    book data;
    node next;
};

typedef struct _list *list;

struct _list
{
    node head;
    int size;
};

void printMenu();//Function that print the basic menu to user

void print(book b);//Function that print a specific book

list load(char *filename);  //Function that recover books list from a savefile

void save(char *filename, list bList);  //Function that saves books list to a savefile

int addBook(book b, list bList);//Function that adds a new book to list

book findBook(book b, list bList); //Function that searches books list to find if a specific book exist.Search procedure based on books id.

int deleteBook(book b, list bList); //Function that deletes a book from list.Delete procedure based on books id.

int updateBook(book b, list bList);  //Function that updates books data.Update procedure based on books id.

book createBook(book a,int id);//Function that gets data from user and creates the book structure

int clean_stdin(void);//Function that cleans the standard input

#endif