#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "books.h"
#include "list.h"

void printMenu(void)//Function that prints the basic menu to user
{
 printf("%s","\n1 to add a book.\n2 to search if a book is registered.\n3 to delete a book.\n4 to update a book.\n5 to print a specific book.\n6 to print all registered books.\n7 to end.\n");
}

int clean_stdin(void)//Function that cleans the standard input and returns 1 to suffice some specific conditions 
{
    while (getchar()!='\n');
    return 1;
}


int addBook (book b , list bList)//Functions that adds a new book based to id,!please keep in my mind that book b parameter that arrives to this function created by function createbook(check below)
{//The base concept taken from the very best book of harvey deitel and paul deitel c programming 7th edition
    int m = 0;
    node newnode = (node)malloc(sizeof(struct _node));

    if(newnode != NULL)
    {   
        node prevPtr;//Pointer to the previous node in the list
        node curPtr;//Pointer to the current node in the list
        
        strcpy(newnode -> data.author,b.author);//data to node
        strcpy(newnode -> data.title,b.title);//data to node
        newnode -> data.genre = b.genre;//data to node
        newnode -> data.id = b.id;//data to node
        memcpy(newnode -> data.reviews,b.reviews,sizeof(b.reviews));//data to node
//Here func_flag becomes true maloc worked node added
        m = 1;//that value signals main() that there is new changes that must be saved to save file
//Here size of list increases
    bList -> size ++;
//Here is the connection setup for the linked list
       prevPtr = NULL;
       curPtr = bList -> head;
       while( curPtr != NULL && newnode -> data.id > curPtr -> data.id )//registers the book sorted in ascending way 
       {
           prevPtr = curPtr;
           curPtr = curPtr -> next;
       }

       if( prevPtr == NULL)//insert node at the start of the list
       {
           newnode -> next = bList -> head;
           bList -> head = newnode;
       } else {//insert node between two others
            prevPtr -> next = newnode;
            newnode -> next = curPtr;
       }
    } else {
        return m;
    }
    return m;//That value that returns to main signs that change occured and need to save that change
}


book findBook( book b, list bList )//Function that search if a book exist on the list based on id
{
    node current = NULL;
    int m = 0;
    current = bList -> head;
    while( current != NULL)
    {
        if( current -> data.id == b.id  )
        {
            strcpy( b.author,current -> data.author );
            strcpy( b.title,current -> data.title );
            b.genre = current -> data.genre;
            memcpy( b.reviews , current -> data.reviews ,sizeof(current -> data.reviews) );
            m = 1;
            break;
        }
        current = current -> next;
    }
    if(m)
    {
         printf("\nBook with id = %d found with success!\n",b.id);//prints appropriate messages to user
    
    } else {
        printf("\n ERROR:: book with id = %d not found,check your id again\n",b.id);
    }
   
    return b;//Returns the whole book structure to main()
}


int deleteBook(book b,list bList)//Function that deletes a specific book based on id
{//The base concept taken from the very best book of harvey deitel and paul deitel c programming 7th edition
    node prevPtr;
    node curPtr;
    node tmpPtr;
    int m = 0;//Value that signals main() that that changes occured and must be saved to savefile

    if( b.id == bList -> head -> data.id)//delete an item at the start of the list
    {
        tmpPtr = bList -> head;
        bList -> head = bList -> head -> next;
        free( tmpPtr );
        bList -> size--;
        m = 1;
    } else {
        prevPtr = bList -> head;
        curPtr = bList -> head -> next;

    while( curPtr != NULL && curPtr -> data.id != b.id)//search untill find the correct position
    {
        prevPtr = curPtr;
        curPtr = curPtr -> next;
    }

    if( curPtr != NULL)
    {//now the correct postion found and delete the item
        tmpPtr = curPtr;
        prevPtr -> next = curPtr -> next;
        free ( tmpPtr );
        bList -> size--;
        m = 1;
    }
    }
    return m;//that value now returns to main()
}

int updateBook(book b, list bList)//Function that updates books data
{
 node current = NULL;
 int choice_menu = 0 , genr = 0 ,id, id_flag = 0 ,mem_flag = 0, review_choice = 0 ,choice_menu_rev , counter = 0;
 int m = 0 , i = 0 , flag = 0;
 char c,choice,w;

 current = bList -> head;
 while( current != NULL && current -> data.id != b.id )//that loop searches all the list until id matches with the id from b.id that came from main function from function bookFind 
 {//if id matched current pointer points to the node that we want to update its components
     current = current -> next;
 }
 if(current != NULL)
 {
         printf("%s","Please select a number from the menu below\n1 to update authors name.\n2 to update books title.\n3 to change books genre.\n4 to update books id.\n5 to update books reviews\n6 to end the process");
         do{
            printf("%s"," ?");
          } while ( ( (scanf("%d%c",&choice_menu,&c)!=2 || c!='\n') && clean_stdin() ) );//Protections from wrong input
     while( choice_menu != 6)
     {
         if( choice_menu == 1 )//Here program gives the user the ability to update books author name
         {
            puts("You chosen to update authors name");
            printf("Current authors name is [%s]\n",b.author);
            printf("%s","Do you wish to continue updating the current authors name\n");
            printf("%s","Answer with y (for yes) or n (for no)\n");
            do{    
                printf("%s","? ");
                scanf(" %c",&choice);
                choice = (char)tolower(choice);
                if( choice != 'y' && choice != 'n' ){
                    printf("%s","Please answer only with y (for yes) or n (for no)\n");
                    clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                }
            }while(choice != 'y' && choice != 'n');//Wrong input protection
            if( choice == 'y' )
            {
                flag = 1;//Flag that signlas main() that change occured and there is new data to be saved
                puts("Please enter the new authors name");
                printf("%s","-> ");
                memset(b.author,0,sizeof(b.author));//Here i clean the previous content of author[MAXSTRING] array with memset.
                clean_stdin();//Here program cleans the standar input because if \n is still in stdin will conflict with the proccess above
                m = 0;
                while ( (w = getchar()) != '\n' && m < MAXSTRING)//The concept here is to use getchar() to get users input,nothing special
                {//didn't want to use scanf because 1)its dangerous to buffer over flow 2)don't read spaces 3)fgets(b.author,MAXSTRING,stdin) is an alternative solution but i like complex codes(Thats my sin)             
                    b.author[m++] = w;//so i created this beautiful code to read everything and print message if user exeeds the limit of allowed charcters
                    if(m == MAXSTRING-1)//buffer overflow protection,stores the first 1023 chars based to  MAXSTRING
                    {
                    b.author[m] = '\0';
                    mem_flag = 1;
                    clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                    break;//stops the loop and m = MAXSTRING -1 (remmber array start counting from zero),here \0 inserted  to the last cell of the array
                    }
                }
                if(m != MAXSTRING-1)
                {
                    b.author[m] = '\0';
                }
                strcpy( current -> data.author , b.author );
                printf("Now authors name is [%s]\n",b.author);
                if(mem_flag)
                puts("Attention,you entered 1023 characters,no more allowed");
         }
         } else if (choice_menu == 2)//Here programm gives user the ability to update books title
         {
            puts("You chosen to update books title\n");
            printf("Current books title is [%s]\n",b.title);
            printf("%s","Do you wish to continue updating the current books title\n");
            printf("%s","Answer with y (for yes) or n (for no)\n");
            do{    
                printf("%s"," ?");
                scanf(" %c",&choice);
                choice = (char)tolower(choice);
                if( choice != 'y' && choice != 'n' ){
                    printf("%s","Please answer only with y (for yes) or n (for no)\n");
                    clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                }
            } while (choice != 'y' && choice != 'n');//wrong input protection
            if( choice == 'y' )
            {
                flag = 1;//Flag that signals main() that change occured and there is new data to be saved
                puts("Please enter the new books title");
                printf("%s"," ->");
                memset(b.title,0,sizeof(b.title));
                clean_stdin();
                m = 0;
                while ( (w = getchar()) != '\n' && m < MAXSTRING)//same concept like above
                {                
                    b.title[m++] = w;
                    if(m == MAXSTRING-1)
                    {
                        b.title[m] = '\0';
                        mem_flag = 1;
                        clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                        break;//stops the loop and m = MAXSTRING -1 (remmber array start counting from zero),here \0 inserted  to the last cell of the array
                    }
                }
                if(m != MAXSTRING-1)
                {
                    b.title[m] = '\0';
                }
                strcpy( current -> data.title , b.title );
                printf("Now authors books title is [%s]\n",b.title);
                if(mem_flag)
                puts("Attention,you entered 1023 characters,no more allowed");
            }
         } else if( choice_menu == 3 )//Here program gives the user the ability to update books genre
         {
            puts("You chosen to update books genre");
            printf("%s","Do you wish to continue updating the current books genre\n");//Questions the user if he/she wants to continue nothing special
            printf("%s","Answer with y (for yes) or n (for no)\n");
            do{    
                printf("%s"," ?");
                scanf(" %c",&choice);
                choice = (char)tolower(choice);
                if( choice != 'y' && choice != 'n' ){
                    printf("%s","Please answer only with y (for yes) or n (for no)\n");
                    clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                }
            } while (choice != 'y' && choice != 'n');//Protection from wrong input
            if( choice == 'y' )
            {
                flag = 1;//Flag that signals main that change occured
                do{
                printf("Please enter the genre of the book.\n1 for Fiction.\n2 for Scientific.\n3 for Politics.\n");
                do{
                    printf("%s","? ");
                } while ( ( (scanf("%d%c", &genr, &c)!=2 || c!='\n') && clean_stdin() ) );//Protection from wrong input
                switch(genr)
                {
                    case 1:
                        printf("So,Ficiton is the type of book? answer with y (for yes) and (n for no)");
                        do{
                            printf("%s"," ?");
                            scanf(" %c",&choice);
                            choice = (char)tolower(choice);
                            if( choice != 'y' && choice != 'n' ){
                                printf("%s","Please answer only with y (for yes) or n (for no)\n");
                                clean_stdin();
                            }
                        }while( choice != 'y' && choice != 'n');//wrong input protection
                    if( choice == 'y')
                    current -> data.genre = FICTION;
                    break;

                case 2:
                    printf("So,Scientific is the type of book? answer with y (for yes) and (n for no)");
                    do{    
                        printf("%s"," ?");
                        scanf(" %c",&choice);
                        choice = (char)tolower(choice);
                        if( choice != 'y' && choice != 'n' ){
                            printf("%s","Please answer only with y (for yes) or n (for no)\n");
                            clean_stdin();
                        }
                }while( choice != 'y' && choice != 'n');//wrong input protection
                if( choice == 'y')
                current -> data.genre = SCIENTIFIC;
                break;

                case 3:
                    printf("So,Politics is the type of book? answer with (y for yes) and (n for no)");
                    do{
                        printf("%s"," ?");
                        scanf(" %c",&choice);
                        choice = (char)tolower(choice);
                        if( choice != 'y' && choice != 'n' ){
                            printf("%s","Please answer only with y (for yes) or n (for no)\n");
                            clean_stdin();
                        }
                }while( choice != 'y' && choice != 'n');//wrong input protection
                current -> data.genre = POLITICS;
                break;

                default:
                    printf("Incorrect choice,please select a number from the menu");
                    break;
            }
            }while(choice == 'n' || genr > 3 || genr < 1);
            }
         } else if( choice_menu == 4)//Here program gives the user the ability to update books id
         {
            puts("You chosen to update books id");
            printf("Current books id is [%d]\n",current -> data.id);
            printf("%s","Do you wish to continiue updating the current books id\n");
            printf("%s","Answer with y (for yes) or n (for no)\n");
            do{    
                printf("%s"," ?");
                scanf(" %c",&choice);
                choice = (char)tolower(choice);//Converting uppercase letters to lowercase to suffice the condition above 
                if( choice != 'y' && choice != 'n'){
                    printf("%s","Please answer only with y or n");
                    clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                }
            } while (choice != 'y' && choice != 'n');//wrong input protection
            if( choice == 'y' )
            {
                flag = 1;    //flag that signals main that change occured
                do
                {
                    puts("Please enter the new id for the book");
                    do{
                        printf("%s"," ?");
                    } while ( ( (scanf("%d%c", &id, &c)!=2 || c!='\n') && clean_stdin() ) );
                    id_flag = list_search_id(id,bList);//Searching the id with search_list_id function,to found if the id that user inserted already exist
                } while(id_flag);//The implementation of list_search_id function described on list.c file
            current -> data.id = id;
            printf("Now books id updated to [%d]\n",current -> data.id);
            }
         } else if(choice_menu == 5)//Program here gives user the ability to manipulate books reviews
         {
            puts("\nYou chosen to update books reviews");
            for(i = 0 ; i < MAXREVIEWS ; i++)
            {
                if(current -> data.reviews[i][0] != '\0')
                counter++;
            }//counter holds the number of reviews,empty reviews skipped!!!!
            if( counter == 0 )//That condition means that no reviews entered or user entered  an empty review
            {
                puts("There are no reviews registered,only first option (add reviews) is available now");//Program informs the user that he/she can add a review only.
                puts("Do you wish to continue adding a new review");
                printf("Answer with y (for yes) or n (for no)\n");
                    do{
                        printf("%s"," ?");
                        scanf(" %c",&choice);
                        choice = (char)tolower(choice);
                    if( choice != 'y' && choice != 'n'){
                        printf("%s","Please answer only with y or n");
                        clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                    }
                   }while( choice != 'y' && choice != 'n' );//wrong input protection                            
                 if( choice == 'y' )
                 {
                     flag = 1;//Flag that returned from update book function and signals change to book,so that save function save the unsaved changes
                     i = 0;
                     m = 0;
                     while( current -> data.reviews[i][0] != '\0' )//Searching the reviews[MAXREVIEWS][0] array,checking always first column to see if there is \0,to identify the empty slot and write at this point the review   
                     {
                         i++;
                     }
                     puts("Please enter the new review");
                     printf("%s"," ->");
                     clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                     while ( (w = getchar()) != '\n' && m < MAXSTRING )//Getting users input
                     {                   
                         current -> data.reviews[i][m++] = w;//Here i variable defined from while loop above that searches arrays empty slots,that procedure gives the program the oprtunity to write reviews sorted even if user inserts an empty review between two noraml reviews(reviews that conatin string no space)! 
                         if(m == MAXSTRING-1)
                         {
                            current -> data.reviews[i][m] = '\0';
                            mem_flag = 1;
                            clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                            break;//stops the loop and m = MAXSTRING -1 (remmber array start counting from zero),here \0 inserted  to the last cell of the array
                        }
                    }
                    if(m != MAXSTRING-1)
                    {
                    current -> data.reviews[i][m] = '\0';;
                    }
                     printf("Review [%s] added with success!\n",current -> data.reviews[i]);
                    if(mem_flag)
                    puts("Attention,you entered 1023 characters,no more allowed");
                 }
            } else {//That condition means that there are existing reviews and gives the user the ability to manipulate choosing from a menu the apπropriate mode 
                printf("%s","Please select one option from the menu below\n1 to add a new review.\n2 to remove all reviews.\n3 to update a specific review.\n4 to remove a specific review.\n5 to end.\n");
                do{
                    printf("%s"," ?");
                } while ( ( (scanf("%d%c", &choice_menu_rev,&c)!=2 || c!='\n') && clean_stdin() ) );//protection from wrong input
                while( choice_menu_rev != 5 )
                {
                    puts("Current book reviews");
                    for(i = 0 ; i < MAXREVIEWS ; i++)
                    {
                        if(current -> data.reviews[i][0] == '\0')
                        continue;//skips empty reviews
                        printf("Review [%d] : %s\n",i+1,current -> data.reviews[i]);//Printing all the reviews that insrted for the specific book
                    }//i holds the number of reviews

                    if(choice_menu_rev == 1)//Here program gives user the ability to add a new review
                    {
                        printf("You chosen to add a new review\n");
                        printf("Do you wish to continue answer with y (for yes) or n (for no)\n");
                        do{
                            printf("%s"," ?");
                            scanf(" %c",&choice);
                            choice = (char)tolower(choice);
                            if( choice != 'y' && choice != 'n'){
                                printf("%s","Please answer only with y or n");
                                clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                            }
                        }while( choice != 'y' && choice != 'n' );//wrong input protection                           
                    if( choice == 'y' )
                    {
                        flag = 1;//Flag that signlas main() that change occured
                        i = 0;
                        m = 0;
                        while( current -> data.reviews[i][0] != '\0' )//Serching the 2d reviews array for empty slots,slots that may also conntain empty reviews from user
                        {
                            i++;
                        }
                        puts("Please enter the new review");
                        printf("%s"," ->");
                        clean_stdin();
                        while ( (w = getchar()) != '\n' && m < MAXSTRING )//Getting input from user
                        {                   
                            current -> data.reviews[i][m++] = w;
                            if(m == MAXSTRING-1)
                            {
                                current -> data.reviews[i][m] = '\0';
                                mem_flag = 1;
                                clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                                break;//stops the loop and m = MAXSTRING -1 (remmber array start counting from zero),here \0 inserted  to the last cell of the array
                            }
                        }
                        if(m != MAXSTRING-1)
                        {
                        current -> data.reviews[i][m] = '\0';;
                        }
                        printf("Review [%s] added with success!\n",current -> data.reviews[i]);
                        if(mem_flag)
                        puts("Attention,you entered 1023 characters,no more allowed");
                    }
                    } else if( choice_menu_rev == 2 )//Here program gives user the abilty to wipe all th reviews
                    {
                        printf("You chosen to remove all the reviews\n");
                        printf("Do you wish to continue answer with y (for yes) or n (for no)\n");
                        do{    
                            printf("%s"," ?");
                            scanf(" %c",&choice);
                            choice = (char)tolower(choice);
                            if( choice != 'y' && choice != 'n'){
                                printf("%s","Please answer only with y or n");
                                clean_stdin();
                            }
                        }while( choice != 'y' && choice != 'n' );//wrong input protection                               
                        if( choice == 'y' )
                        {
                            flag = 1;//Flag that signals that change occured,that value returned from updateBook function to main!
                            memset(current -> data.reviews,0,sizeof(current -> data.reviews));//Setting the whole 2d array to 0,all reviews lost!
                            puts("All reviews removed with success");
                        }
                    } else if( choice_menu_rev == 3 )//Here program gives user the ability to update the content of a specific review
                    {
                        printf("You chosen to update a specific review\n");
                        printf("Do you wish to continue answer with y (for yes) or n (for no)\n");
                        do{
                            printf("%s"," ?");
                            scanf(" %c",&choice);
                            choice = (char)tolower(choice);
                        if( choice != 'y' && choice != 'n'){
                            printf("%s","Please answer only with y or n");
                            clean_stdin();
                        }
                    }while( choice != 'y' && choice != 'n' ); //wrong input protection                                 
                    if( choice == 'y' )
                    {
                        flag = 1;//Flag that signals that change ocuured,that value returns to main
                        printf("Please select the number of the review you want to update from the menu above (number inside the brackets)\n");
                        do{    
                            printf("%s"," ?");
                        } while ( ( (scanf("%d%c",&review_choice,&c)!=2 || c!='\n') && clean_stdin() ) || review_choice <= 0 || review_choice > MAXREVIEWS  );//Protection from wrong user input
                        m = 0;
                        memset(current -> data.reviews[review_choice - 1],0,sizeof(current -> data.reviews[review_choice - 1]));//Removing the old review
                        puts("Please enter the new review");
                        printf("%s"," ->");
                        while ( (w = getchar()) != '\n' && m <MAXSTRING )//Getting users input
                        {                   
                            current -> data.reviews[review_choice - 1][m++] = w;
                            if(m == MAXSTRING-1)
                            {
                                current -> data.reviews[review_choice - 1][m] = '\0';
                                mem_flag = 1;
                                clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                                break;//stops the loop and m = MAXSTRING -1 (remmber array start counting from zero),here \0 inserted  to the last cell of the array
                            }
                        }
                        if(m != MAXSTRING-1)
                        {
                        current -> data.reviews[review_choice - 1][m] = '\0';
                        }
                        printf("Review [%d] updated with success to [%s]\n",review_choice,current -> data.reviews[review_choice - 1]);
                        if(mem_flag)
                            puts("Attention,you entered 1023 characters,no more allowed");
                    }
                    } else if ( choice_menu_rev == 4 )//Here program gives user the ability to remove a specific review
                    {
                        printf("You chosen to remove a specific  review\n");
                        printf("Do you wish to continue answer with y (for yes) or n (for no)\n");
                        do{
                            printf("%s"," ?");
                            scanf(" %c",&choice);
                            choice = (char)tolower(choice);
                            if( choice != 'y' && choice != 'n'){
                                printf("%s","Please answer only with y or n");
                                clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                            }
                        }while( choice != 'y' && choice != 'n' );                                    
                        if( choice == 'y' )
                        {   
                            flag = 1;//flag that signals main() that cjange occured
                            printf("Please select the number of the review you want to remove from the menu above(number inside the brackets)\n");
                            do{
                            printf("%s"," ?");
                            } while ( ( (scanf("%d%c",&review_choice,&c)!=2 || c!='\n') && clean_stdin() ) || review_choice <= 0 || review_choice > MAXREVIEWS  ); 
                            memset(current -> data.reviews[review_choice - 1],0,sizeof(current -> data.reviews[review_choice - 1]));//Removing the reviews that user selected using function memset,beautiful function. i <3 that function!
                            printf("Review with number [%d] removed with success\n",review_choice);
                        }     
                        } else {
                            puts("Incorrect choice try again");
                        }
                        printf("%s","Please select a number from the menu below\n1 to add a new review.\n2 to remove all reviews.\n3 to update a specific review.\n4 to remove a specific review.\n5 to end.\n");
                        do{
                            printf("%s"," ?");
                        } while ( ( (scanf("%d%c", &choice_menu_rev,&c)!=2 || c!='\n') && clean_stdin() ) );//wrong input protection
                }
            }
        } else {
                puts("Incorrect choice");
        }
        printf("%s","Please select a number from the menu below\n1 to update authors name.\n2 to update books title.\n3 to change genre.\n4 to update books id.\n5 to update books review\n6 to end the process");
        do{
        printf("%s"," ?");
        } while ( ( (scanf("%d%c", &choice_menu,&c)!=2 || c!='\n') && clean_stdin() ) );
     }
}
return flag;
}

void print(book b)//Function that prints a specific book based on id
{
    int i = 0;
    if( b.genre != 0){//!!!!Enstablishes that book id exist and don't print just an empty book,book b parameter that arrives to printBook function contains an empty book variable and  b.genre == 0.Enum genre consant start counting from 1 see enum struct at books.h
        printf("Books author : %s\n",b.author);//Thats how i undestand that the book is not an empty book.I didnt use the id parameter because user may insert books id = 0 or a negative id.Extreme scenario but my goal is to cover even the worst case scenario. 
        printf("Books title : %s\n",b.title);
        if( b.genre == FICTION)
        {
            printf("%s","Books genre is fiction\n");
        } else if( b.genre == SCIENTIFIC )
        {
            printf("%s","Books genre is scientific\n");
        } else if ( b.genre == POLITICS )
        {
            printf("%s","Books genre is politics\n");
        }
        printf("Books id is [%d]\n",b.id);

        for( i=0 ; i < MAXREVIEWS ; i++)
        {
            if(b.reviews[i][0] == '\0')
            continue;
            printf("Books review [%d] -> %s\n",i+1,b.reviews[i]);
        }
    } else {
        printf("No book printed,probaly no id matched check for error message\n");
    }
}





book createBook(book a,int id)//Function that creates the book structure
{
    int genr,m = 0,mem_flag = 0,j = 0;
    char c;
    char choice,rev_choice;
    
    a.id = id;
    
    printf("Welcome my friend ,you entering information for book with id = %d \n",a.id);
    printf("Keep in mind that only 1023 charcters allowed for each text segment\n");
    printf("Please enter authors name:\n");
    printf("%s"," ?");
    m = 0;
    while ( (c = getchar()) != '\n' && m < MAXSTRING)//Getting input from user,nothing special explained and above at updateBook function,user here enter books author
        {                
            a.author[m++] = c;
            if(m == MAXSTRING-1)
            {
                a.author[m] = '\0';
                puts("Attention,you entered 1023 characters,no more allowed");
                clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                break;//stops the loop and m = MAXSTRING -1 (remmber array start counting from zero),here \0 inserted  to the last cell of the array
            }
        }
        if(m != MAXSTRING-1)
        {
             a.author[m] = '\0';
        }

        printf("Please enter books title\n");
        printf("%s"," ?");
         m=0;
         while ( (c = getchar()) != '\n' && m < MAXSTRING)//Getting input from user,user enter here books title
        {                
            a.title[m++] = c;
            if(m == MAXSTRING-1)
            {
                a.title[m] = '\0';
                puts("Attention,you entered 1023 characters,no more allowed");
                clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                break;//stops the loop and m = MAXSTRING -1 (remmber array start counting from zero),here \0 inserted  to the last cell of the array
            }
        }
        if(m != MAXSTRING-1)
        {
             a.title[m] = '\0';
        }
        
        do{//User here chooses from a menu the genre of the book
            printf("Please enter the genre of the book.\n1 for Fiction.\n2 for Scientific.\n3 for Politics.\n");
            
            do{
                printf("%s"," ?");
            } while ( ( (scanf("%d%c", &genr, &c)!=2 || c!='\n') && clean_stdin() ) );//Protection from wrong input
            switch(genr)
            {
                case 1:
                printf("So,Ficiton is the type of book? answer with y (for yes) and (n for no)");
                do{
                    printf("%s"," ?");
                    scanf(" %c",&choice);
                    choice = (char)tolower(choice);
                    if( choice != 'y' && choice != 'n' ){
                        printf("%s","Please answer only with y (for yes) or n (for no)\n");
                        clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                    }
                }while( choice != 'y' && choice != 'n');//Protection from wrong input,nothing special
                if( choice == 'y' )
                 a.genre = FICTION;
                break;

                case 2:
                printf("So,Scientific is the type of book? answer with y (for yes) and (n for no)");
                do{
                    printf("%s"," ?");
                    scanf(" %c",&choice);
                    choice = (char)tolower(choice);
                    if( choice != 'y' && choice != 'n' ){
                        printf("%s","Please answer only with y (for yes) or n (for no)\n");
                        clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                    }
                }while( choice != 'y' && choice != 'n');
                if( choice == 'y' )
                 a.genre = SCIENTIFIC;
                break;

                case 3:
                printf("So,Politics is the type of book? answer with (y for yes) and (n for no)");
                do{
                    printf("%s"," ?");
                    scanf(" %c",&choice);
                    choice = (char)tolower(choice);
                    if( choice != 'y' && choice != 'n' ){
                        printf("%s","Please answer only with y (for yes) or n (for no)\n");
                            clean_stdin();
                    }
                }while( choice != 'y' && choice != 'n');
                if( choice == 'y' )
                 a.genre = POLITICS;
                break;

                default:
                    printf("Incorrect choice,please select a number from the menu");
                    break;
            }
            }while(choice == 'n' || genr > 3 || genr < 1);

        
        printf("\nIf you wish you can write a review for the book\n");//Asking user if he/she want to write a review for the book
        printf("Remember that only 10 reviews can be written for each book seperately\n");//Printing crucial information to user
        do{
            if( j == 0 )//Print this message only the first time,next time that one review allready added print just the neccesary information not all this messages
            {
            printf("%s","\nDo you wish to write a review for the book answer with y (for yes) or n (for no)");
            do{
            printf("%s"," ?");
            scanf(" %c",&rev_choice);
            rev_choice = (char)tolower(rev_choice);
            if( rev_choice != 'y' && rev_choice != 'n' ){
                printf("%s","Please answer only with y (for yes) or n (for no)\n");
                clean_stdin();
            }
            } while( rev_choice != 'y' && rev_choice != 'n');//wrong input protection
            }
        if( rev_choice == 'y')
        {
            printf("%s","Now you can enter your review\n");
            printf("%s"," ->");
            m = 0;
            clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
            while ( (c = getchar()) != '\n' && m <MAXSTRING )//Getting input from user
            {                
                a.reviews[j][m++] = c;//Here j represent the number of the review
                if(m == MAXSTRING-1)
                {
                    a.reviews[j][m] = '\0';
                    mem_flag = 1;
                    clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                    break;//stops the loop and m == MAXSTRING-1
                }
            }
            if(m != MAXSTRING-1)
            {
                a.reviews[j][m] = '\0';
            }
            j++;//Here j variable increases,that means that a review added,now j points to the second row of the 2d reviews array,and this procedure continues until j become equal to MAXREVIEWS constant then no more reviews allowed
            if(mem_flag)
                printf("%s","String exeeds the limit,only 1023 characters stored at this review\n");
            printf("%s","Do you wish to continue,answer with y or n\n");
                do{
                printf("%s"," ?");
                scanf(" %c",&rev_choice);
                rev_choice = (char)tolower(rev_choice);
                if( rev_choice != 'y' && rev_choice != 'n' ){
                    printf("%s","Please answer only with y (for yes) or n (for no)\n");
                    clean_stdin();//Cleans the standard input from characters that may leave and conflict next user input
                }
                } while( rev_choice != 'y' && rev_choice != 'n');//Protection from wrong input,nothing special
            }
        } while(rev_choice == 'y'  && j < MAXREVIEWS);
        
        return a;//Returns to main function the book structure that created,so other function like addBook can use this data
}

void save(char *filename, list bList)//Function that saves user data
{
    FILE *fp;
    int m = 0;
    node current;
    if( ( fp = fopen(filename,"wb") ) == NULL)
    {
        printf("File could not be opened.");
    } else
    {
        current = bList -> head;
        while( current != NULL)//Accessing the whole list and save each nodes data seperately to a save file
        {
            m = 1;
            fwrite( current -> data.author , sizeof(current -> data.author) , 1 , fp );
            fwrite( current -> data.title , sizeof(current -> data.title) , 1 , fp );
            fwrite( &(current -> data.genre) , sizeof(current -> data.genre) , 1 , fp );
            fwrite( &(current -> data.id) , sizeof(current -> data.id) , 1 , fp );
            fwrite( current -> data.reviews , sizeof(current -> data.reviews), 1 ,fp );
            current = current -> next;
        }
        fclose(fp);
        if(m)
        printf(" :Data saved succesfully to file [%s]\n",filename);
    }
}

list load(char *filename)//Function that loads users data from a save file
{
    FILE *fp;
    char author[MAXSTRING],title[MAXSTRING];
    int id;
    enum genres gen;
    char reviews[MAXREVIEWS][MAXSTRING];
    int m = 0;
    size_t n;
    list l;
    l = list_init();
    if(( fp = fopen(filename,"rb") ) == NULL)
    {
        puts("File could not be opened\n");
    } else
    {
    book b;
       
        while(1)//Loop that reads all the data from a savefile
        {
                n = fread( author , sizeof(char) , sizeof(b.author) ,fp);//fread returns to n variable the number of elements successfully read from the save file
                if (n < sizeof(b.author) )//if n variable is less than the size of MAXSTRING(thats the size of b.author the size of characters that can be insrted on author array)  the loop,breaks that means EOF reached or fread encountered error!
                break;//In other words i take advantage of  the fread function return value to determine if i reached End Of File
                strcpy( b.author , author);
                fread( title , sizeof(char) , sizeof(b.title) ,fp);
                strcpy( b.title,title);
                fread( &gen , sizeof(b.genre) , 1 ,fp);
                b.genre = gen;
                fread( &id, sizeof(b.id) , 1 ,fp);
                b.id = id;
                fread( reviews , sizeof(char) , sizeof(b.reviews) ,fp);
                memcpy(b.reviews,reviews,sizeof(reviews));
                addBook(b,l);
        }
        printf("%s","Books list recovered successfully from save file\n");
        fclose(fp);
    }
        return l;
}//The reason i dont use feof() function explained here better at this link,i make a research because feof function didn't work properly:http://faq.cprogramming.com/cgi-bin/smartfaq.cgi?answer=1046476070&id=1043284351
